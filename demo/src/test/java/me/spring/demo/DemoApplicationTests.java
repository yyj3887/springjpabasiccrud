package me.spring.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.servlet.ServletContext;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import me.spring.demo.Controller.CustomerController;

@SpringBootTest
class DemoApplicationTests {

	MockMvc mc;

	@Autowired
	CustomerController cc;

	@Autowired
	ServletContext servletContext;

	@BeforeEach
	void beforetest() {
		mc = MockMvcBuilders.standaloneSetup(cc).build();
	}

	@Test
	void contextLoads() throws Exception {
		RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/customer/1").contentType(MediaType.APPLICATION_JSON);
		this.mc.perform(reqBuilder).andExpect(status().isOk()).andDo(print());

		// final String a = servletContext.getRealPath("/resources");
		// final String pullpath = a + "\\tiger.jpg";
		// final File file = new File(pullpath);
		// final byte[] bytes = new byte[(int) file.length()];
		// final DataInputStream dataInputStream = new DataInputStream(new
		// FileInputStream(file));
		// dataInputStream.readFully(bytes);
		// dataInputStream.close();
		// assertEquals(bytes.length, 47755);
	}
}
