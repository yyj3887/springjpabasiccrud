package me.spring.demo.Controller;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lombok.NoArgsConstructor;
import me.spring.demo.Model.Customer.Customer;
import me.spring.demo.Service.biz.CustermerService;

@CrossOrigin
@RestController
@RequestMapping("/")
@NoArgsConstructor
public class CustomerController {
    @Autowired
    CustermerService cs;

    @Autowired
    ServletContext servletContext;

    // customer 전부 출력
    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<Customer>> getAllFind() {
        return new ResponseEntity<List<Customer>>(cs.findAll(), HttpStatus.OK);
    }

    // Id에 해당하는 customer 출력
    @GetMapping(value = { "/customer/{id}" }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Customer> getbyIdFind(@PathVariable("id") int id) {
        return new ResponseEntity<Customer>(cs.findbyid(id), HttpStatus.OK);
    }

    // Id에 해당하는 customer 삭제
    @DeleteMapping(value = { "/customer/{id}" })
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        cs.delete(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    // Id에 해당하는 customer 변경
    @PutMapping(value = { "/customer/{id}" }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Customer> update(@PathVariable("id") int id, @RequestBody Customer customer) {
        cs.update(id, customer);
        return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
    }

    // Id에 Id2에 사이값 조회
    @GetMapping(value = { "/customer/{id}/{id2}" }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<Customer>> findByIdBetween(@PathVariable("id") int id, @PathVariable("id2") int id2) {
        return new ResponseEntity<List<Customer>>(cs.findByIdBetween(id, id2), HttpStatus.OK);
    }

    // image파일 응답 전송
    @GetMapping(value = { "/image/{filename}" }, produces = { MediaType.IMAGE_JPEG_VALUE })
    public ResponseEntity<byte[]> findimage(@PathVariable("filename") String id) throws IOException {
        String a = servletContext.getRealPath("/resources");
        String pullpath = a + "\\tiger.jpg";
        File file = new File(pullpath);
        byte[] bytes = new byte[(int) file.length()];
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
        dataInputStream.readFully(bytes);
        dataInputStream.close();

        return new ResponseEntity<byte[]>(bytes, HttpStatus.OK);
    }

    @PostMapping(value = { "/upload" }, produces = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public void upload(@RequestPart MultipartFile files) {
        // 파일명
        String originalFile = files.getOriginalFilename();
        // 파일명 중 확장자만 추출 //lastIndexOf(".") - 뒤에 있는 . 의 index번호
        String originalFileExtension = originalFile.substring(originalFile.lastIndexOf("."));

        System.out.println(originalFile + originalFileExtension);
    }
}