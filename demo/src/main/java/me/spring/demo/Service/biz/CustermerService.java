package me.spring.demo.Service.biz;

import java.util.List;

import me.spring.demo.Model.Customer.Customer;

public interface CustermerService {
    public List<Customer> findAll();

    public void Save(Customer customer);

    public Customer findbyid(int id);

    public void update(int id, Customer customer);
    // public List<Customer> findBetWeenCustomers(int id1, int id2);

    public void delete(int id);

    public List<Customer> findByIdBetween(int id, int id2);
}