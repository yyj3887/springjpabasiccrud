package me.spring.demo.Service.biz;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.spring.demo.Model.Customer.Customer;
import me.spring.demo.Repository.Customer.CustomerRepository;

@Service
public class CustermerServiceImpl implements CustermerService {

    @Autowired
    CustomerRepository cusomerrepostitory;

    @Override
    public void Save(Customer customer) {
        this.cusomerrepostitory.save(customer);
    }

    @Override
    public void delete(int id) {
        this.cusomerrepostitory.deleteById(id);
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();
        this.cusomerrepostitory.findAll().forEach(e -> customers.add(e));
        return customers;
    }

    @Override
    public Customer findbyid(int id) {
        return this.cusomerrepostitory.findById(id).get();
    }

    @Override
    public void update(int id, Customer customer) {
        Customer cs = this.findbyid(id);
//        cs.setName(customer.getName());
        this.cusomerrepostitory.save(cs);
    }

    @Override
    public List<Customer> findByIdBetween(int id, int id2) {
        return this.cusomerrepostitory.findByIdBetween(id, id2);
    }
}