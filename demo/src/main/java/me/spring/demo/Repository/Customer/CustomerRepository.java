package me.spring.demo.Repository.Customer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import me.spring.demo.Model.Customer.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    List<Customer> findByIdBetween(int id, int id2);
}