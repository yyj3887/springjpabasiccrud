package me.spring.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

import me.spring.demo.Model.Customer.Customer;
import me.spring.demo.Repository.Customer.CustomerRepository;

@SpringBootApplication
@EntityScan(basePackages = "me.spring.demo.*")
@ComponentScan(basePackages = "me.spring.demo.*")
public class DemoApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CustomerRepository cr;

	@Override
	public void run(String... args) throws Exception {
		cr.save(new Customer("양영재"));
		cr.save(new Customer("꿈룡"));
		cr.save(new Customer("류냐"));
		cr.save(new Customer("즌짱"));
		cr.save(new Customer("혼암"));
		cr.save(new Customer("칠숙"));
		cr.save(new Customer("덕만"));
		cr.save(new Customer("얼음"));
		cr.save(new Customer("미륵"));
		cr.save(new Customer("북타"));
		cr.save(new Customer("방울"));
		cr.save(new Customer("모카"));
		cr.save(new Customer("마루"));
	}

}
